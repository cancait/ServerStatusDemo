package com._Config;

import com.ModularTest.machineState.machineStateControlle;
import com.ModularTest.textFusing.fusingControlle;
import com.jfinal.config.Routes;



public class Config_AdminRoutes extends Routes{
	/**
	 * 这是问题库的配置文件
	 * 每个链接后面必须，写上注释。表示就是一个目录
	 */
	@Override
	public void config() {
//		访问路径“/hello”	//访问类“hello.class”
		add("/",machineStateControlle.class);//演示路径
		add("/fusing",fusingControlle.class);//测试熔断
		
		
		
	}
}
