package com._Config;

import com.jfinal.server.undertow.UndertowServer;

public class MainService {
/*	1：极速启动，启动速度比 tomcat 快 5 到 8 倍。jfinal.com 官网启动时间在 1.5 秒内 

	2：Undertow 是红帽公司的开源产品，是 Wildfly 默认的 Web 服务器，Java Web 服务器市场占有率高于 Jetty，仅次于 Tomcat

	3：极简精妙的热部署设计，实现极速轻量级热部署，让开发体验再次提升一个档次

	4：性能高于 tomcat、jetty，可替代 tomcat、jetty 用于生产环境

	5：undertow 为嵌入式而生，可直接用于生产环境部署，部署时无需下载服务，无需配置服务，十分适合微服务开发、部署

	6：告别 web.xml、告别 tomcat、告别 jetty，节省大量打包与部署时间。令开发、打包、部署成为一件开心的事

	7：功能丰富，支持 classHotSwap、WebSocket、gzip 压缩、servlet、filter、sessionHotSwap 等功能 

	8：支持 fatjar 与 非 fatjar 打包模式，便于支持微服务

	9：开发、打包、部署一体化，整个过程无需对项目中的任何地方进行调整或修改，真正实现从极速开发到极速部署*/

	public static void main(String[] args) {
		UndertowServer.start(Config.class);
	}
	
	
//	****************************特别注意****************************
//	****************************特别注意****************************
//	****************************特别注意****************************
//	Undertow 是为嵌入而生的 Web Server，web.xml 已被抛弃，所以无法通过 web.xml 配置 web 组件。
//	为此 jfinal undertow 提供了 UndertowServer.configWeb(...) 可以很方便添加 Filter、WebSocket、Servlet、Listener 这些标准的 Java Web 组件：
/*	UndertowServer.create(AppConfig.class)
.configWeb( builder -> {
    // 配置 Filter
    builder.addFilter("myFilter", "com.abc.MyFilter");
    builder.addFilterUrlMapping("myFilter", "/*");
    builder.addFilterInitParam("myFilter", "key", "value");
    
    // 配置 Servlet
    builder.addServlet("myServlet", "com.abc.MyServlet");
    builder.addServletMapping("myServlet", "*.do");
    builder.addServletInitParam("myServlet", "key", "value");
    
    // 配置 Listener
    builder.addListener("com.abc.MyListener");
    
    // 配置 WebSocket，MyWebSocket 需使用 ServerEndpoint 注解
    builder.addWebSocketEndpoint("com.abc.MyWebSocket");
 })
.start();*/
//	****************************特别注意****************************
//	****************************特别注意****************************
//	****************************特别注意****************************
}
