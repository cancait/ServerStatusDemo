package com._Config;

import com.Modular.fusingPlugin.Fusing;
import com.Modular.fusingPlugin.FusingPlugin;
import com.Modular.lognPlugin.errorLogPlugin;
import com._Config.configUtil.DruidKit;
import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.Const;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;
import com.mysql.jdbc.Connection;




public class Config extends JFinalConfig{
	
	
	/**
	 * 启动入口，运行此 main 方法可以启动项目，此 main 方法可以放置在任意的 Class 类定义中，不一定要放于此
	 * /ServerStatus/src/main/resources/undertow.txt  便是配置启动类   的配置文件
	 */
	public static void main(String[] args) { UndertowServer.start(Config.class); }

	private static Prop p = loadConfig();
	public static Prop ppConfig = p;
	private WallFilter wallFilter;
	/**
	 * 配置常量
	 */
	@Override
	public void configConstant(Constants me) {
		me.setDevMode(p.getBoolean("devMode", false));//开发者模式
//		me.setViewType(ViewType.FREE_MARKER);//默认视图类型freemarker
		me.setEncoding("utf-8");//设置编码
		me.setError401View("/errorPage/400.html");//%%%%下%%%%没有JAR包的话，访问会报错%%%%下%%%%
		me.setError403View("/errorPage/400.html");//报错的跳转页
		me.setError404View("/errorPage/400.html");//报错的跳转页
		me.setError500View("/errorPage/400.html");//%%%%上%%%%没有JAR包的话，访问会报错%%%%上%%%%
		me.setMaxPostSize(100*Const.DEFAULT_MAX_POST_SIZE);//默认10M,此处设置为最大1000M
//		me.setBaseDownloadPath(baseDownloadPath);//设置文件下载基础路径参数 "/var/www/download" 为绝对路径，下载文件存放在此路径之下
//		me.setBaseUploadPath(p.get("UploadPath").trim());// 设置文件上传保存基础路径
	}
	/**
	 * 配置访问路由
	 * 路由拆分到 FrontRutes 与 AdminRoutes 之中配置的好处：
     * 1：可分别配置不同的 baseViewPath 与 Interceptor
     * 2：避免多人协同开发时，频繁修改此文件带来的版本冲突
     * 3：避免本文件中内容过多，拆分后可读性增强
     * 4：便于分模块管理路由
	 */
	@Override
	public void configRoute(Routes me) {
		me.add(new Config_FrontRoutes());// 前端路由
		me.add(new Config_AdminRoutes());// 后端路由
	}
    /**
     * 配置模板引擎，通常情况只需配置共享的模板函数
     */
	@Override
	public void configEngine(Engine me) {
//		me.setDevMode(p.getBoolean("engineDevMode", false));
//		me.setBaseTemplatePath(PathKit.getWebRootPath());//设置基准路径
//		me.addSharedFunction("tool/NewFile.html");//把页面中的方法分享到全局中，所有页面都可以直接调用，不需要页面使用#include("NewFile.html")<!-- //引入页面函数的指令 -->#set(a=["菜单1","菜单2","菜单3"])   如：/*		#for(x:a) <a>#(x)</a><br/> #end   #define aaa(as) 我是,#(as) #end*/
//		me.addSharedStaticMethod(enginedemo.class);//该代码的公共属性，页面全局共享，通过#(str())---->#(方法名)---调用，只共享方法，不共享参数，否则请把参数写在方法中return i；
//		me.addSharedObject("ctx", JFinal.me().getContextPath());//带项目名路径需要配置改参数	页面上 项目 #(ctx)就可以了      如果用的 jfinal enjoy ， 不再需要 ContextHandler ， 而是直接  这样性能更好
	}

	/**
	 * 配置插件
	 */
	@Override
	public void configPlugin(Plugins me) {
//		NacosServicePlugin nacosPlugin = new NacosServicePlugin("127.0.0.1", "csq.c", "11.11.11.22", 80, 1000);
//		me.add(nacosPlugin);
//		NacosConfigPlugin nacosconfigPlugin = new NacosConfigPlugin("127.0.0.1", "11", "22");
//		me.add(nacosconfigPlugin);
		
		FusingPlugin fusingPlugin = new FusingPlugin(3, 1000,Ret.create().set("code", "500").set("data", "少年一会请求吧").toJson());
		me.add(fusingPlugin);
		
		DruidPlugin druidPlugin = getDruidPlugin();
		wallFilter = new WallFilter(); // 加强数据库安全
		wallFilter.setDbType("mysql");
		druidPlugin.addFilter(wallFilter);
		druidPlugin.addFilter(new StatFilter()); // 添加 StatFilter 才会有统计数据
		me.add(druidPlugin);

		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);

		arp.setTransactionLevel(Connection.TRANSACTION_READ_COMMITTED);// 事物等级
		_MappingKit.mapping(arp);// 表关系分到map中，方便管理
		me.add(arp);
		if (p.getBoolean("devMode", false)) {// 控制台显示执行的SQL
			arp.setShowSql(true);
		}

		
	    /**↓↓↓↓↓↓↓↓↓↓报错案例 //指定SQL文件//多个文件不同的配置。但是KEY必须唯一。否则报错#sql("demoa2")↓↓↓↓↓↓↓↓↓↓
		 * Sql already exists with key : demoaid
			Template: "/sql/demo2.sql". Line: 7
			com.jfinal.template.stat.ParseException: Sql already exists with key : demoaid
			Template: "/sql/demo2.sql". Line: 7    
			↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑报错案例 ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑*/
        
/*		arp.setBaseSqlTemplatePath(PathKit.getWebRootPath()+"/WEB-INF");//设置SQL（设置SQL起始文件夹）基准的路径setBaseSqlTemplatePath、、获取当前的WebRoot 路径 PathKit.getWebRootPath()
		arp.addSqlTemplate("/sql/demo.sql");//指定SQL文件//多个文件不同的配置。但是KEY必须唯一。否则报错#sql("demoa2")
		arp.addSqlTemplate("/sql/demo2.sql");//指定SQL文件//多个文件不同的配置。但是KEY必须唯一。否则报错#sql("demoa2")*/
//		me.add(arp);
//		arp.addMapping("goods_biao", Feedback.class);//具体的表类  演示

        //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓redis  用于缓存platform 模块的内容↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
//        RedisPlugin redisPlugin = new RedisPlugin("platform", p.get("redis.url").trim());//platform
//        me.add(redisPlugin);
		//↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑redis  用于缓存platform 模块的内容↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
        
        
	}
	/**
	 * 设置全局拦截器
	 */
	@Override
	public void configInterceptor(Interceptors me) {
		me.addGlobalActionInterceptor(new Fusing());//全局熔断
//		me.addGlobalActionInterceptor(new errorLogPlugin());//全局异常捕获
//		me.addGlobalActionInterceptor(new POST());//全局//post
//		me.addGlobalActionInterceptor(new interceptors_dactionAll());//全局//案例路径com.Zduangduang
//		me.addGlobalServiceInterceptor(new interceptors_duang());//针对全局的service层 duang。例：Service_duang2 duang = Duang.duang(Service_duang2.class);  duang.方法();
	}
	/**
	 * 配置Handler。管理者
	 */
	@Override
	public void configHandler(Handlers me) {
	    me.add(DruidKit.getDruidStatViewHandler()); // druid 统计页面功能
//		me.add(new UrlSeoHandler());             	// index、detail 两类 action 的 url seo//SEO是指通过站内优化比如网站结构调整、网站内容建设、网站代码优化等以及站外优化
	    
//	    me.add(new ContextPathHandler("ctx"));//html提交带项目名路径需要配置改参数	页面上 项目 #(ctx)不就可以了   遗弃，使用 jfinal enjoy configEngine()
	}
	
    /**
     * 抽取成独立的方法，例于 _Generator 中重用该方法，减少代码冗余
     */
	public static DruidPlugin getDruidPlugin() {
		return new DruidPlugin(p.get("jdbc.url").trim(), p.get("jdbc.username").trim(), p.get("jdbc.password").trim());
	}
	/**
	 * 加载配置文件
	 */
	private static Prop loadConfig() {
		//根据系统加载配置文件
		String os = System.getProperty("os.name");  
		if(os.toLowerCase().startsWith("win")){ 
			//加载开发环境配置文件
			return PropKit.use("_config_dev.txt");
		}else {
			// 优先加载生产环境配置文件jboot.properties
			return PropKit.use("_config_pro.txt");
		}

	}
	
	
	/**
	 * 框架启动的时候，启动某些奇怪的东西，比如线程什么的，
	 */
/*	public void afterJFinalStart(){
		new 线程();
	};*/
	/**
	 * 框架结束的时候，结束某些奇怪的东西，比如删库与跑路什么的，
	 */
/*	public void beforeJFinalStop(){
		删除数据库().跑路();
	};*/

}
