package com.Modular.lognPlugin;

import com._Config.Model.errorLogModel;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.json.JSONUtil;

public class errorLogPlugin implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		 try {
		        inv.invoke();  
		    } catch (Exception e) {
		    	StackTraceElement[] stackTrace = e.getStackTrace();
		    	String jsonString = JSONUtil.parseObj( inv.getController().getRequest().getParameterMap()).toJSONString(0);
		    	
		    	new errorLogModel().set("message_id", "127.0.0.1"+UUID.fastUUID())//位移标识:服务器ID+UUID
		    	.set("service_id", "127.0.0.1")//服务器ID
		    	.set("class_name", stackTrace[0].getClassName()+stackTrace[0].getFileName())//报错类名+方法名
		    	.set("method_name", stackTrace[0].getMethodName())//方法名
		    	.set("parameter_content", jsonString)//传递参数
		    	.set("errorContent", e.getMessage())//异常内容
		    	.set("create_time",DateUtil.date())//时间
		    	.set("line_number", stackTrace[0].getLineNumber())//报错行号
		    	.save();
		    	//这里捕获到的异常，可以丢入MQ之中，因为我的是demo所以就没有用MQ，直接存储到MYSQL了
		    }
	}

}
